package com.dhfr.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceRestTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceRestTemplateApplication.class, args);
	}
}
